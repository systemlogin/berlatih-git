@extends('layout.master')
@section('judul')
    Halaman Detail Cast {{$cast->nama}}
@endsection
@section('content')
<h1 class="text-primary" style="text-transform:uppercase;">{{$cast->nama}}, {{$cast->umur}} tahun</h1>
<p>{{$cast->bio}}</p>
@endsection
