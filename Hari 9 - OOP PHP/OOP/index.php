<?php 
require('animal.php');


$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


"<br>";
$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>"; 
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold blooded : " . $kodok->cold_blooded . "<br>"; 
echo "Jump : " . $kodok->jump . "<br><br>";// "hop hop"

// index.php
$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>"; // "shaun"
echo "Legs : " . $sungokong->legs . "<br>"; // 4
echo "Cold blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
echo "Yell : " . $sungokong->yell . "<br>";// "Auooo"

?>